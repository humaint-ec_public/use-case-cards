# Use case cards

<img src="Images/UCC.png" alt="The Use Case Card project" class="center">

## Description

<b>Use case cards</b> is an initiative of the [HUMAINT project](https://ai-watch.ec.europa.eu/humaint_en) at the Joint Research Center of the European Commission. It provides a unified methodology for the <b>documentation of AI use cases</b> inspired by the [European AI Act](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:52021PC0206).

<img src="Images/template.png" alt="Use Case Card template" class="center">


## Roadmap

We are now working on collecting a comprehensive unified catalogue of AI use cases following the <i>use case cards</i> template. In the future, we plan to develop a web-based prototype of this registry integrating a machine-editable version of <i>use case cards</i> and allowing for the automated analysis of related statistics such as the number of use cases per application area, per product type and most covered SDGs.

## Acknowledgment

If you use our template or data, please reference the following works:

<ul>
  <li>Hupont, I., Fernández-Llorca, D., Baldassarri, S. & Gómez, E. (2023). Use case cards: a use case reporting framework inspired by the European AI Act. [Link to preprint.](https://arxiv.org/pdf/2306.13701.pdf)</li>
  <li>Hupont, I., & Gómez, E. (2022). Documenting use cases in the affective computing domain using Unified Modeling Language. 10th IEEE International Conference on Affective Computing and Intelligent Interaction (ACII). [Link to preprint.](https://arxiv.org/pdf/2209.09666.pdf)</li>
</ul>

